package steps;

import com.epam.ta.businesobject.InboxBO;
import com.epam.ta.driverfactory.DriverManager;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class ManageEmail {
    InboxBO inboxBO;

    @When("I click on the Undo button")
    public void iClickOnTheUndoButton() {
        inboxBO.cancelDeleteSelectedEmails();
    }

    @Then("Emails are not removed from Inbox")
    public void emailsAreNotRemovedFromInbox() {
        Assert.assertTrue(inboxBO.isInboxListChanged(),"Inbox list of emails are not the same.");
    }

    @And("^I delete (\\d{1}) emails$")
    public void iDeleteNumberOfEmailsEmails(int numberOfEmails) {
        inboxBO = new InboxBO();
        inboxBO.deleteEmails(numberOfEmails);
    }
    @After
    public void AfterScenario(){
        DriverManager.quitDriver();
    }

}
