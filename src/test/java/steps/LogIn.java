package steps;

import com.epam.ta.businesobject.LogInBO;
import io.cucumber.java.en.Given;

public class LogIn {
    LogInBO loginBO;

    @Given("^I log in with (\\w+@\\w+.\\w+) and (.*)$")
    public void iLogInWithUsernameAndPassword(String userName, String password) {
        loginBO = new LogInBO();
        loginBO.logIn(userName,password);
    }
}
