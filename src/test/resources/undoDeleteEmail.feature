Feature: Operation With Emails

  Scenario Outline: Undo deleting email operation
    Given I log in with <username> and  <password>
    And I delete <numberOfEmails> emails
    When I click on the Undo button
    Then Emails are not removed from Inbox

    Examples:
      | username                      | password |numberOfEmails|
      | automationtest388@gmail.com   | 4esZXdr5 |3|
