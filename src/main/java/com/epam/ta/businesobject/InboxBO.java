package com.epam.ta.businesobject;

import com.epam.ta.pageobject.GmailInboxPage;

public class InboxBO {
    private GmailInboxPage gmailInboxPage;

    public InboxBO(){
        gmailInboxPage = new GmailInboxPage();
    }
    public void deleteEmails(int numberOfEmails){
        gmailInboxPage.selectEmailCheckBox(numberOfEmails);
        gmailInboxPage.deleteEmails();
    }
    public void cancelDeleteSelectedEmails(){
        gmailInboxPage.undoDeleteEmail();
    }

    public boolean isInboxListChanged() {
        boolean isListNotChanged = gmailInboxPage.initialEmailList.equals(gmailInboxPage.getAllEmails());
        return isListNotChanged;
    }
}
