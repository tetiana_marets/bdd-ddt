package com.epam.ta.businesobject;

import com.epam.ta.pageobject.GmailLoginPage;

public class LogInBO {
    private GmailLoginPage gmailLoginPage;

    public LogInBO(){
        gmailLoginPage = new GmailLoginPage();
    }
    public void logIn(String userName, String userPassword){
        gmailLoginPage.typeUserEmail(userName);
        gmailLoginPage.submitEmail();
        gmailLoginPage.typeUserPassword(userPassword);
        gmailLoginPage.submitPassword();
    }
}
