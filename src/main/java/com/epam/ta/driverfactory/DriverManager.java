package com.epam.ta.driverfactory;

import org.openqa.selenium.WebDriver;

public class DriverManager {
    private static ThreadLocal<WebDriver> driverPool = new ThreadLocal<>();

    private DriverManager(){

    }

    public static WebDriver getDriver(DriverType driverType){
        if (driverPool.get() == null) {
            driverPool.set(new DriverFactory().createDriver(driverType));
        }
        return driverPool.get();
    }

    public static void quitDriver(){
        if (driverPool != null) {
            driverPool.get().quit();
            driverPool.set(null);
        }
    }
}
