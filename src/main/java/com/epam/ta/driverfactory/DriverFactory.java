package com.epam.ta.driverfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private static final int implicitlyWaitTime = 10;
    private static final TimeUnit timeUnit = TimeUnit.SECONDS;
    private static final String INITIAL_URL = "https://accounts.google.com/AccountChooser?service=mail&continue=https://mail.google.com/mail/";

    public WebDriver createDriver (DriverType driverType) {
        WebDriver webDriver;
        if (driverType == DriverType.CHROME) {
            System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
            webDriver = new ChromeDriver();
            webDriver.manage().window().maximize();
            webDriver.manage().timeouts().implicitlyWait(implicitlyWaitTime,timeUnit);
            webDriver.get(INITIAL_URL);
        }
        else {
            throw new NotImplementedException();
        }
        return webDriver;
    }
}
