package com.epam.ta.pageobject;

import com.epam.ta.driverfactory.DriverManager;
import com.epam.ta.driverfactory.DriverType;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractPage {
    AbstractPage() {
        PageFactory.initElements(DriverManager.getDriver(DriverType.CHROME),this);
    }
}
