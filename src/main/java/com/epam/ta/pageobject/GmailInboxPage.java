package com.epam.ta.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GmailInboxPage extends AbstractPage {
    @FindBy(className = "zF")
    List<WebElement> allEmails;

    @FindBy(xpath = "//*[contains(@role,'checkbox')]")
    List<WebElement> checkBoxes;

    @FindBy(xpath = "//*[contains(@class, 'T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs mA')]")
    WebElement deleteButton;

    @FindBy(xpath = "//span[@id='link_undo']")
    WebElement undoButton;

    public List<WebElement> initialEmailList;

    public GmailInboxPage() {
        initialEmailList = getAllEmails();
    }

    public void deleteEmails() {
        deleteButton.click();
    }

    public void undoDeleteEmail(){
        undoButton.click();
    }

    public List<WebElement> getAllEmails() {
        return allEmails;
    }

    public void selectEmailCheckBox(int numberOfEmailsToSelect) {
        if (numberOfEmailsToSelect >= 1) {
            for (int i = 1; i <= numberOfEmailsToSelect; i++){
                checkBoxes.get(i).click();
            }
        }
        else {

        }
    }
}
